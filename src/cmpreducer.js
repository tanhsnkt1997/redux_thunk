/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button } from 'react-native';
import * as actions from './action/index';
import { connect } from 'react-redux';
import Child from './child'

class cmpreducer extends React.Component {


    handleIncrease = () => {
        this.props.counterIncerase();
    };

    handleDecrease = () => {
        this.props.counterDecrease();
    };




    render() {
        console.log('================>', this.props)
        return (
            <View style={styles.container} >
                <Text style={styles.textCounter}>COUTER</Text>
                <View style={{ flexDirection: 'row-reverse' }}>

                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Button
                            title="Increase"
                            textColor="#fff"
                            color="#397af8"
                            onPress={this.handleIncrease} />
                    </View>

                    <Child />

                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Button
                            title="Decrease"
                            color="orange"
                            onPress={this.handleDecrease} />
                    </View>
                </View>
            </View >
        );
    }
}

export default connect(null, actions)(cmpreducer);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    textCounter: {
        fontSize: 25,
    }
});
