import { INCREASE, DECREASE } from '../action/type'
//ham quan ly cac state lien quan den task
const initialState = 0;


/*
const taskReducer = (task = [], actiob) => {
  switch (action.type) {
    case INCREASE:
      return [
        ...task,
        {
          taskId: action.taskId
        }
      ]

    case DECREASE:
      return state - 1;

    default:
      return state;
  }
}
export default taskReducer
*/


export default function (state = initialState, action) {
  switch (action.type) {
    case INCREASE:
      return state + 1;

    case DECREASE:
      return state - 1;

    default:
      return state;
  }
}

