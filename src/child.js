import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { AsyncStorage } from 'react-native';


class Child extends Component {
    constructor(props) {
        super(props);
        console.log("props==========>", this.props);

    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.counter !== this.props.counter) {
            return true
        }
        else {
            return false
        }
    }


    render() {
        console.log('--------------Dang render-----------------')
        return (
            <View style={styles.text}>
                <Text style={styles.text} >{this.props.counter}</Text>
            </View>
        )
    }
}


const mapStateToProps = state => ({
    counter: state.counter
});


export default connect(mapStateToProps, null)(Child);

const styles = StyleSheet.create({
    text: {
        fontSize: 100,
        color: '#000',
        marginLeft: 10,
        marginRight: 10
    }
});